# Thread .NET - mini-project Thread

## Опис проекту:

**Thread** - це готовий шаблон клієнт-серверного додатку, в якому клієнтом виступає Single Page Application ([SPA](https://medium.com/@NeotericEU/single-page-application-vs-multiple-page-application-2591588efe58)), а бекенд - веб-сервіс. Проект має базову архітектуру, спрямовану на подальше розширення. Для вирішення типових задач у проекті використовуються популярні бібліотеки й фреймворки.

Тематика проекту - соціальна мережа, схожа на Twitter.

Основна ідея проекту - ознайомитися з нашим баченням того, як має виглядати реальний проект, розібратися у тому, як влаштована архітектура проекту, подивитися на його можливі конфігурації, спробувати покопатися у чужому коді.

## Технології:

### Backend:

-   [.NET 5](https://dotnet.microsoft.com/download)
-   [SQL Server](https://www.microsoft.com/sql-server/sql-server-downloads)
-   [EF Core](https://docs.microsoft.com/ef/core)
-   [FluentValidation](https://github.com/JeremySkinner/FluentValidation)
-   [AutoMapper](https://github.com/AutoMapper/AutoMapper)
-   [Bogus](https://github.com/bchavez/Bogus)
-   [JWT](https://jwt.io)
-   [SignalR](https://dotnet.microsoft.com/apps/aspnet/real-time)

### Frontend:

-   [Angular](https://angular.io)
-   [Angular Material](https://material.angular.io)

## Рекомендовані інструменти:

-   [Visual Studio IDE](https://visualstudio.microsoft.com/vs)
-   [Visual Studio Code](https://code.visualstudio.com)
-   [Postman](https://www.getpostman.com)